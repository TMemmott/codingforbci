classdef HuffmanCode < DecisionTreeCode
    % builds and traverses huffman trees
    %
    % METHODS:
    %   BuildTree = build a huffman tree (codebook)
    
    properties (Hidden=true)
        % logical, if true will not learn
        freezeFlag
    end
    
    methods
        function self = HuffmanCode(varargin)
            self = self@DecisionTreeCode(varargin{:});
            
            self.freezeFlag = false;
        end
        
        function codebook = BuildTree(self,probM)
            % builds huffman code tree
            %
            % INPUT
            %   probM   = [numSrcMsg x 1] prob of srcMsg
            %
            % OUTPUT
            %   codebook = [numSrcMsg x maxCodewordLength] matrix of
            %              codewords (each row is a codeword).  Note that a
            %              0 is a placeholder value such that each codeword
            %              has the same "length".
             
            if self.freezeFlag
                codebook = self.codebook;
                return
            end
            
            numSrcMsg = length(probM);
            numX = size(self.probYgivenX, 2);          
            
            % build huffman codebook (dictionary)
            % NOTE:  huffmandict in a MATLAB comm toolbox fnc
            dict = huffmandict(1 : numSrcMsg, probM, numX);
            
            % add 1 to idxs to be comaptible
            codebook = cellfun(@(x)(x+1), {dict{:,2}}', ...
                'uniformoutput', false);
            
            % add 0s to ensure each codeword has same length
            maxCodewordLength = max(cellfun(@length, codebook));
            addZeros = @(x)([x, zeros(1, maxCodewordLength - length(x))]);
            codebook = cell2mat(cellfun(addZeros, codebook, ...
                'UniformOutput',false));
        end
    end  
end

