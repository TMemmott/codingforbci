classdef DMCcode < handle
    % superclass for codes on discrete memorlyess channels, by itself it is
    % not a code, see maxInfoCode, huffmanCode, randomCode or uniformCode.
    %
    % In short:
    %
    % M -> X -> Y
    %
    % where M is a srcMsg (right left ... for wheelchair, 'a' ... 'z' for
    % spelling etc) 
    %
    % X/Y are BCI-signals (mSequence/frequency for SSVEP, R/L imagined
    % movement for motor imagery, p300/non-p300).  They are ground truth
    % and estimate respectively.
    %
    % The first mapping from M -> X is the "code" which can encode multiple
    % m_i as a single x_j.  The confusion matrix from X->Y is assumed
    % static and known (probYgivenX)
    %
    % METHODS:
    %
    % ComputeCodeStats = computes the mutual information between M and Y
    %                    and probMgivenY
    % BuildAll = builds all possible code vectors
    
    properties
        % [numChannelOutMsg x numChannelInMsg] confusion
        % matrix of BCI-Channel
        probYgivenX
    end
    
    
    methods
        function self = DMCcode(varargin)
            % constructor
            p = inputParser;
            p.KeepUnmatched = true;   % accepts (ignores) irrelevant inputs
            p.addParameter('probYgivenX',[],@ismatrix);
            p.parse(varargin{:});
            
            self.probYgivenX =p.Results.probYgivenX;
        end
        
        function [MI, probMgivenY] = ComputeCodeStats(self, code, probM)
            % computes code statistics given a particular code vector and
            % source probability
            %
            % INPUT
            %   code = [numSrcMsg x numCodes] idx of brain symbol
            %          associated with each source message 
            %   probM = [numSourceMsg x 1] prob of srcMsg
            %
            % OUTPUT
            %   MI  = [numCodes x 1] mutual info between M and Y (in bits)
            %   probMgivenY = [numSrcMsg x numX x numCodes], conditional
            %                 distribution of srcMsg given brainSymbol (for
            %                 each code)
            %
            % EVENT 1: we use idx 0 in a code to indicate that a srcMsg is
            % not shown during the trial (inactive).  Its probability is
            % held constant in probMgivenY.  If you're interested in using
            % the likelihoods of all the presented srcMsgs to infer about
            % those not shown, please define a null brainSymbol which the
            % code can map all not shown srcMsgs to.  Including it in this
            % way offers cleaner encapsulation as X is always defined for
            % all M
            
            % find dimensions
            numCodes = size(code, 2);
            numSrcMsg = length(probM);
            numX = size(self.probYgivenX, 1);
            probM = probM(:);
            
            assert(size(code, 1) == numSrcMsg, 'invalid code dim');
            
            % pre-allocate
            MI = NaN(numCodes, 1);
            probMgivenY = NaN(numSrcMsg, numX, numCodes);
            
            for codeIdx = 1 : numCodes
                code_ = code(:, codeIdx);
                assert(all(code_), 'zero idx no longer supported');
                
                probY = nan(size(self.probYgivenX, 1), 1);
                for yIdx = 1 : length(probY)
                   probY(yIdx) = sum(probM(code_ == yIdx)); 
                end
                               
                probXY = self.probYgivenX * diag(probY);
                
                % note MI(x, y) = MI(m, y)
                MI(codeIdx) = MutualInfo(probXY);
                
                probMgivenY_ = nan(numSrcMsg, numX);
                for mIdx = 1 : numSrcMsg
                    probMgivenY_(mIdx, :) = ...
                        self.probYgivenX(:, code_(mIdx)) * probM(mIdx);
                end
                
                probMgivenY(:, :, codeIdx) = ...
                    probMgivenY_ * diag(1 ./ sum(probMgivenY_));
                
                % replace nan rows with orig probability distribution
                % (error arises because probY = 0 given code, if any mass
                % does go to probY, we just use prior distribution)
                usePriorIdx = ~sum(probMgivenY_) | ...
                    isnan(sum(probMgivenY_)) | isinf(sum(probMgivenY_));
                
                for yIdx = find(usePriorIdx)
                    probMgivenY(:, yIdx, codeIdx) = probM;
                end
            end
            
            assert(~any(isnan(MI(:))) && ~any(isnan(probMgivenY(:))), ...
                'invalid output');
        end
        
        function allCodes = BuildAll(self, numSourceMsg)
            % builds all possible codes by counting
            % INPUT
            %   numSourceMsg = scalar, number of source messages, |M|
            % OUTPUT
            %   allCodes          = [numSourceMsg x numPossibleCodes]
            %                       list of all possible codes (each is a
            %                       column vector)
            
            numX = size(self.probYgivenX, 2);
            totalNumberOfCodes = numX ^ numSourceMsg;
            
            if totalNumberOfCodes > 1e7
                error('total number of codes > 1e7');
            elseif totalNumberOfCodes > 1e6
                warning('total number of codes > 1e6');
            end
            
            % counting in appropriate base, 000, 001, 010, 011, 100 ...
            allCodesChar = ...
                dec2base(0 : totalNumberOfCodes - 1, numX, numSourceMsg)';
            
            % convert to double
            allCodes = char(allCodesChar) - double('0') + 1;
        end
    end
end

