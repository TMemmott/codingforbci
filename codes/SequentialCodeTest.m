%% SequentialCodeTest.m

%% parameters
clearvars; clc;
numSrcMsg = 10;   
numX = 6;

%% build arbitrary input distributions - non square confusion matrix
randDist = @(L)(diff([0; sort(rand(L - 1, 1), 'ascend'); 1]));

for xIdx = numX : -1 : 1
    probYgivenX(:, xIdx) = randDist(numX);
end

probM = randDist(numSrcMsg);

%% test
sequentialCodeObj = SequentialCode('probYgivenX',probYgivenX);

% build sequential code vector
[code, probMgivenY, MI] = sequentialCodeObj.Build(probM);

%% test - build one at a time (traverse tree)
targetMsgIdx = randi(numSrcMsg);
allCodes = code;
while any(code)
    % build probX as certain ground truth
    xTarget = code(targetMsgIdx);
    probX = zeros(numX,1);
    probX(xTarget) = 1;
    
    code = sequentialCodeObj.Build(probM,... 
        'probX', probX);
    
    if any(code)
        allCodes = [allCodes, code]; %#ok<AGROW>
    end
end

%% display code traverse
disp(sequentialCodeObj.codebook);