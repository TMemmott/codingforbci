function [MI, varargout] = GetCapacity(probYgivenX)
% approximates (local max) the capacity of a DMC channel and, optionally,
% gives the input distribution which achieves this capacity
% 
% INPUT
%   probYgivenX = [numY x numX] confusion matrix of BCI-Channel
%
% OUTPUT
%   MI          = scalar, channel capacity (max mutual info) (bits)
%   probX       = [numX x 1] distribution over X which achieves max MI

numX = size(probYgivenX, 2);

% initial point (uniform)
x0 = ones(numX, 1) / numX;

% obj fnc
fnc = @(x)(-MutualInfo(probYgivenX * diag(x)));

% inequality constraint, all probs are positive
A = -eye(numX);
b = zeros(numX, 1);

% equality constraint, all probs sum to 1
Aeq = ones(1, numX);
beq = 1;

% perform optimization
options = optimoptions('fmincon','Display','off');
probX = fmincon(fnc, x0, A, b, Aeq, beq, [], [], [], options);

% prep outputs
MI = -fnc(probX);
varargout{1} = probX;
end

